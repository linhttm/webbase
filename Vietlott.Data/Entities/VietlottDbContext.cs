﻿using Microsoft.EntityFrameworkCore;
using System;

namespace Vietlott.Data.Entities
{
    public class VietlottDbContext : DbContext
    {
        public VietlottDbContext()
        {
        }
        public VietlottDbContext(DbContextOptions<VietlottDbContext> options) : base(options)
        {
        }
        public DbSet<MessageEnvelope> MessageEnvelopes { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Lottery> Lotteries { get; set; }
        public DbSet<UserLottery> UserLotteries { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            var optionsBuilder = new DbContextOptionsBuilder<VietlottDbContext>();
            new VietlottDbContext(optionsBuilder.Options);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserLottery>().HasKey(ul => new { ul.UserId, ul.LotteryId });

            modelBuilder.Entity<UserLottery>()
                .HasOne<User>(ul => ul.User)
                .WithMany(u => u.UserLotteries)
                .HasForeignKey(ul => ul.UserId);

            modelBuilder.Entity<UserLottery>()
                .HasOne<Lottery>(ul => ul.Lottery)
                .WithMany(l => l.UserLotteries)
                .HasForeignKey(ul => ul.LotteryId);
        }
    }
}
