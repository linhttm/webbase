﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vietlott.Data.Entities
{
    public class User : EntityBase
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Cmnd { get; set; }
        public string Password { get; set; }
        public IList<UserLottery> UserLotteries { get; set; }
    }
}
