﻿using HashidsCore.NET;
using System;
using System.ComponentModel.DataAnnotations;

namespace Vietlott.Data.Entities
{
    public abstract class EntityBase
    {
        public EntityBase()
        {
            Id = (new Hashids(Guid.NewGuid().ToString("N"))).Encode(1, 2, 3, 4);
            CreatedDateUtc = ConvertToUnixTimestamp(DateTime.UtcNow);
        }
        [MaxLength(250)]
        public string Id { get; set; }
        public long? CreatedDateUtc { get; set; }
        public string CreateBy { get; set; }
        [MaxLength(250)]
        public string UpdateBy { get; set; }
        public long? UpdateDateUtc { get; set; }

        private static DateTime _epoch = new DateTime(1970, 1, 1);
        public static DateTime ConvertFromUnixTimestamp(long ms)
        {
            return _epoch.AddMilliseconds(ms);
        }
        public static long ConvertToUnixTimestamp(DateTime date)
        {
            var ts = new TimeSpan(date.Ticks - _epoch.Ticks);
            return (long)ts.TotalMilliseconds;
        }
    }
}
