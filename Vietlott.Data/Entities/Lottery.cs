﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Vietlott.Data.Entities
{
    public class Lottery : EntityBase
    {
        [Column(TypeName = "jsonb")]
        public ICollection<int> TicketNumbers { get; set; }
        public List<long> LotteryPeriod { get; set; }
        public IList<UserLottery> UserLotteries { get; set; }
    }
}
