﻿using System.ComponentModel.DataAnnotations;

namespace Vietlott.Data.Entities
{
    public class MessageEnvelope : EntityBase
    {
        public MessageEnvelope() : base()
        {
            FailureCount = 1;
        }
        [MaxLength(50)]
        public string Message { get; set; }
        public string ErrorMessage { get; set; }
        public string MessageBody { get; set; }
        public string MessageType { get; set; }
        [MaxLength(50)]
        public string QueueName { get; set; }
        public long? LastFailedAt { get; set; }
        public int FailureCount { get; set; }
        [MaxLength(50)]
        public string FailureType { get; set; }
        [MaxLength(20)]
        public string ReferenceId { get; set; }
    }
    public enum MessageFailureType
    {
        Send,
        Receive
    }
}
