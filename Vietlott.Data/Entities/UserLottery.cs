﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vietlott.Data.Entities
{
    public class UserLottery : EntityBase
    {
        public string UserId { get; set; }
        public User User { get; set; }
        public string LotteryId { get; set; }
        public Lottery Lottery { get; set; }
    }
}
