﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vietlott.Business.Lotteries.Models
{
    public class LotteryHistoryByUserViewModel 
    {
        public string SubmitAt { get; set; }
        public List<string> LotteryPeriod { get; set; }
        public ICollection<int> TicketNumbers { get; set; }
    }
}
