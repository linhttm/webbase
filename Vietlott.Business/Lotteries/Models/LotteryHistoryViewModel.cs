﻿using System.Collections.Generic;

namespace Vietlott.Business.Lotteries.Models
{
    public class LotteryHistoryViewModel : UserSubmitViewModel
    {
        public string SubmitAt { get; set; }
        public ICollection<int> TicketNumbers { get; set; }
    }
}
