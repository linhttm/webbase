﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vietlott.Business.Lotteries.Models
{
    public class UserSubmitViewModel
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Cmnd { get; set; }
    }
}
