﻿using System;
using System.Collections.Generic;
using System.Text;
using Vietlott.Data.Entities;

namespace Vietlott.Business.Lotteries.Models
{ 
    public class SubmitLotteryViewModel 
    {
        public List<List<int>> Lotteries { get; set; }
        public string SubmitAt { get; set; }
        public string TotalMoney { get; set; }
        public UserSubmitViewModel UserModel { get;  set; }
    }
}
