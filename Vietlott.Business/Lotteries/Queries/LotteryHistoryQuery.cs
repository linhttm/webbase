﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using Vietlott.Business.Lotteries.Models;
using Vietlott.Core.Caching;
using Vietlott.Core.Constants;
using Vietlott.Core.Extensions;
using Vietlott.Core.Interfaces;
using Vietlott.Core.Sessions;
using Vietlott.Data.Entities;

namespace Vietlott.Business.Lotteries.Queries
{
    public interface ILotteryHistoryQuery : IQuery<LotteryHistoryRequest, LotteryHistoryResponse>
    {

    }

    public class LotteryHistoryQuery : SessionComponentBase, ILotteryHistoryQuery
    {
        public LotteryHistoryQuery(IAppSession session, ICacheEngine cacheEngine) : base(session, cacheEngine)
        {

        }
        public LotteryHistoryResponse Invoke(LotteryHistoryRequest request)
        {
            if (request.Name.IsNullOrEmpty() && request.Phone.IsNullOrEmpty() && request.Date.IsNullOrEmpty())
            {
                return new LotteryHistoryResponse
                {
                    Status = CommandResponseStatus.Failed,
                    Message = LotteryConstants.Message.FailedHistory,
                };
            }
            if (request.Date.IsNotNullOrEmpty() && request.Date.TryParseString())
            {
                return new LotteryHistoryResponse
                {
                    Status = CommandResponseStatus.Failed,
                    Message = LotteryConstants.Message.FailedHistory,
                };
            }
            var list = Session.VietlottDbContext.UserLotteries.Include(x => x.Lottery).Include(x => x.User).AsEnumerable();
            list = request.Name.IsNotNullOrEmpty() ? list.Where(x => x.User.Name == request.Name) : list;
            list = request.Phone.IsNotNullOrEmpty() ? list.Where(x => x.User.Phone == request.Phone) : list;
            list = request.Date.IsNotNullOrEmpty() ? list.Where(
                        x => x.CreatedDateUtc.Value.ConvertFromUnixTimestamp().ToFormatString() == request.Date).ToList() : list;
            if(list.Count() == 0)
            {
                return new LotteryHistoryResponse
                {
                    Status = CommandResponseStatus.NotFound,
                    Message = LotteryConstants.Message.NoHistory,
                };
            }
            var lotteries = new List<LotteryHistoryViewModel>();
            foreach (var item in list)
            {
                lotteries.Add(new LotteryHistoryViewModel
                {
                    SubmitAt = item.CreatedDateUtc.Value.ConvertFromUnixTimestamp().ToFormatString(),
                    Name = item.User.Name,
                    Phone = item.User.Phone,
                    TicketNumbers = item.Lottery.TicketNumbers
                });
            }
            return new LotteryHistoryResponse
            {
                Status = CommandResponseStatus.Ok,
                Message = LotteryConstants.Message.History,
                Lotteries = lotteries
            };
        }
    }
    public class LotteryHistoryRequest
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Date { get; set; }
    }
    public class LotteryHistoryResponse : CommandResponseBase
    {
        public List<LotteryHistoryViewModel> Lotteries { get; set; }
    }
}

