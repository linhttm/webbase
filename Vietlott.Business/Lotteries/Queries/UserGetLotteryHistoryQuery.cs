﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vietlott.Business.Lotteries.Models;
using Vietlott.Core.Caching;
using Vietlott.Core.Constants;
using Vietlott.Core.Extensions;
using Vietlott.Core.Interfaces;
using Vietlott.Core.Sessions;
using Vietlott.Data.Entities;

namespace Vietlott.Business.Lotteries.Queries
{
    public interface IUserGetLotteryHistoryQuery : IQuery<UserGetLotteryHistoryRequest, UserGetLotteryHistoryResponse>
    {
    }
    public class UserGetLotteryHistoryQuery : SessionComponentBase, IUserGetLotteryHistoryQuery
    {
        public UserGetLotteryHistoryQuery(IAppSession session, ICacheEngine cacheEngine) : base(session, cacheEngine)
        {

        }
        public UserGetLotteryHistoryResponse Invoke(UserGetLotteryHistoryRequest request)
        {
            if (request.UserId.IsNullOrEmpty())
            {
                return new UserGetLotteryHistoryResponse
                {
                    Status = CommandResponseStatus.Failed,
                    Message = LotteryConstants.Message.FailedHistory,
                };
            }
            var user = Session.VietlottDbContext.Users.FirstOrDefault(x => x.Id == request.UserId);
            if (user == null)
            {
                return new UserGetLotteryHistoryResponse
                {
                    Status = CommandResponseStatus.NotFound,
                    Message = UserContants.Message.UserNotFound,
                };
            }
            var list = Session.VietlottDbContext.UserLotteries.Include(x => x.Lottery).Where(x => x.UserId == request.UserId).ToList();
            if (list.Count == 0)
            {
                return new UserGetLotteryHistoryResponse
                {
                    Status = CommandResponseStatus.NotFound,
                    Message = LotteryConstants.Message.NoHistory
                };
            }
            var lotteries = new List<LotteryHistoryByUserViewModel>();
            foreach (var item in list)
            {
                var listPeriod = new List<string>();
                foreach (var period in item.Lottery.LotteryPeriod)
                {
                    listPeriod.Add(period.ConvertFromUnixTimestamp().ToFormatString());
                }
                lotteries.Add(new LotteryHistoryByUserViewModel
                {
                    SubmitAt = item.CreatedDateUtc.Value.ConvertFromUnixTimestamp().ToFormatString(),
                    TicketNumbers = item.Lottery.TicketNumbers,
                    LotteryPeriod = listPeriod,
                });
            }
            return new UserGetLotteryHistoryResponse
            {
                Status = CommandResponseStatus.Ok,
                Message = LotteryConstants.Message.History,
                Lotteries = lotteries
            };
        }
    }
    public class UserGetLotteryHistoryRequest
    {
        public string UserId { get; set; }
    }
    public class UserGetLotteryHistoryResponse : CommandResponseBase
    {
        public List<LotteryHistoryByUserViewModel> Lotteries { get; set; }
    }
}
