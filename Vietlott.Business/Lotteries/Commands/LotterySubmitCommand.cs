﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Vietlott.Business.Lotteries.Models;
using Vietlott.Core.Caching;
using Vietlott.Core.Constants;
using Vietlott.Core.Extensions;
using Vietlott.Core.Interfaces;
using Vietlott.Core.Sessions;
using Vietlott.Data.Entities;

namespace Vietlott.Business.Lotteries.Commands
{
    public interface ILotterySubmitCommand : ICommand<LotterySubmitRequest, LotterySubmitResponse>
    {
    }
    public class LotterySubmitCommand : SessionComponentBase, ILotterySubmitCommand
    {
        public LotterySubmitCommand(IAppSession session, ICacheEngine cacheEngine)
            : base(session, cacheEngine)
        {
        }
        public LotterySubmitResponse Invoke(LotterySubmitRequest request)
        {
            if (request.TicketNumbers.IsNullOrEmpty() || request.User.Name.IsNullOrEmpty() ||
                request.User.Phone.IsNullOrEmpty() || request.User.Cmnd.IsNullOrEmpty() ||
                request.LotteryPeriod.IsNullOrEmpty())
            {
                return new LotterySubmitResponse
                {
                    Message = LotteryConstants.Message.SubmitLotteryFailed,
                    Status = CommandResponseStatus.Failed
                };
            }
            foreach (var item in request.TicketNumbers)
            {
                if (item.Count() != 6)
                {
                    return new LotterySubmitResponse
                    {
                        Message = LotteryConstants.Message.SubmitLotteryFailed,
                        Status = CommandResponseStatus.Failed
                    };
                }
            }
            var listPeriod = new List<long>();
            foreach (var item in request.LotteryPeriod)
            {
                if (item.Length != 10 || item.TryParseString())
                {
                    return new LotterySubmitResponse
                    {
                        Message = LotteryConstants.Message.SubmitLotteryFailed,
                        Status = CommandResponseStatus.Failed
                    };
                }
                listPeriod.Add(item.GetDateFromString().ConvertToUnixTimestamp());
            }
            var list = new List<List<int>>();
            var user = Session.VietlottDbContext.Users.FirstOrDefault(x => x.Phone == request.User.Phone);
            //kiem tra xem sdt da dung de mua ve truoc do hay chua
            if (user == null)
            {
                Session.VietlottDbContext.Users.Add(request.User);      
                foreach (var item in request.TicketNumbers)
                {
                    var lottery = new Lottery
                    {
                        LotteryPeriod = listPeriod,
                        TicketNumbers = item
                    };
                    list.Add(item);
                    Session.VietlottDbContext.Lotteries.Add(lottery);
                    Session.VietlottDbContext.UserLotteries.Add(new UserLottery
                    {
                        User = request.User,
                        LotteryId = lottery.Id
                    });
                }
            }
            else
            {
                user.Name = request.User.Name;
                user.Cmnd = request.User.Cmnd;
                foreach (var item in request.TicketNumbers)
                {
                    var lottery = new Lottery
                    {
                        LotteryPeriod = listPeriod,
                        TicketNumbers = item
                    };
                    list.Add(item);
                    Session.VietlottDbContext.Lotteries.Add(lottery);
                    Session.VietlottDbContext.UserLotteries.Add(new UserLottery
                    {
                        UserId = user.Id,
                        LotteryId = lottery.Id
                    });
                }
            }
            var lotterymodel = new SubmitLotteryViewModel
            {
                UserModel = Mapper.Map<UserSubmitViewModel>(request.User),
                Lotteries = list,
                SubmitAt = DateTime.UtcNow.ToFormatString(),
                TotalMoney = (list.Count()*10000).ToString()
            };
            return new LotterySubmitResponse
            {
                Message = LotteryConstants.Message.SubmitLotteryOk,
                Status = CommandResponseStatus.Ok,
                LotteryModel = lotterymodel
            };
        }
    }
    public class LotterySubmitRequest
    {
        public List<string> LotteryPeriod { get; set; }
        public User User { get; set; }
        public List<List<int>> TicketNumbers { get; set; }
    }
    public class LotterySubmitResponse : CommandResponseBase
    {
        public SubmitLotteryViewModel LotteryModel { get; set; }

    }
}
