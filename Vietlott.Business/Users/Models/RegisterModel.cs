﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vietlott.Business.Users.Models
{
    public class RegisterModel
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Cmnd { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
