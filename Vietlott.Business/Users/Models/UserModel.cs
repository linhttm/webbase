﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vietlott.Business.Users.Models
{
    public class UserModel
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Cmnd { get; set; }
        public string CreatedDateUtc { get; set; }
    }
}
