using System.Collections.Generic;
using AutoMapper;
using System.Linq;
using Vietlott.Core.Caching;
using Vietlott.Core.Constants;
using Vietlott.Core.Extensions;
using Vietlott.Core.Interfaces;
using Vietlott.Core.Sessions;
using Vietlott.Data.Entities;
using Vietlott.Business.Users.Models;

namespace Vietlott.Business.Users.Commands
{
    public interface IUserGetAllQuery : IQuery<UserGetAllRequest, UserGetAllResponse>
    {
    }
    public class UserGetAllQuery : SessionComponentBase, IUserGetAllQuery
    {
        public UserGetAllQuery(IAppSession session, ICacheEngine cacheEngine) : base(session, cacheEngine)
        {
        }

        public UserGetAllResponse Invoke(UserGetAllRequest request)
        {
            if (string.IsNullOrEmpty(request.Name) && string.IsNullOrEmpty(request.Phone) && string.IsNullOrEmpty(request.Cmnd))
            {
                return new UserGetAllResponse
                {
                    Message = UserContants.Message.UserNotFound,
                    Status = CommandResponseStatus.Failed,
                };
            }
            else
            {
                IQueryable<User> users = Session.VietlottDbContext.Users.OrderBy(x => x.Name);
                users = !string.IsNullOrEmpty(request.Name) ? users.Where(x => x.Name.Contains(request.Name)) : users;
                users = !string.IsNullOrEmpty(request.Phone) ? users.Where(x => x.Phone.Contains(request.Phone)) : users;
                users = !string.IsNullOrEmpty(request.Cmnd) ? users.Where(x => x.Cmnd.Contains(request.Cmnd)) : users;
                if (users.Count() > 0)
                {
                    return new UserGetAllResponse
                    {
                        Message = UserContants.Message.GetUsersOk,
                        Status = CommandResponseStatus.Ok,
                        Users = Mapper.Map<List<UserModel>>(users)
                    };
                }
                else
                {
                    return new UserGetAllResponse
                    {
                        Message = UserContants.Message.UserNotFound,
                        Status = CommandResponseStatus.NotFound,
                    };
                }
            }
        }
    }

    public class UserGetAllRequest
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Cmnd { get; set; }
    }

    public class UserGetAllResponse : CommandResponseBase
    {
        public List<UserModel> Users { get; set; }
    }
}
