using AutoMapper;
using System.Linq;
using Vietlott.Business.Users.Models;
using Vietlott.Core.Caching;
using Vietlott.Core.Constants;
using Vietlott.Core.Extensions;
using Vietlott.Core.Interfaces;
using Vietlott.Core.Sessions;

namespace Vietlott.Business.Users.Commands
{
    public interface IUserGetOneQuery : IQuery<UserGetOneRequest, UserGetOneResponse>
    {
    }
    public class UserGetOneQuery : SessionComponentBase, IUserGetOneQuery
    {
        public UserGetOneQuery(IAppSession session, ICacheEngine cacheEngine) : base(session, cacheEngine)
        {
        }
        public UserGetOneResponse Invoke(UserGetOneRequest request)
        {
            if (string.IsNullOrEmpty(request.UserId))
            {
                return new UserGetOneResponse
                {
                    Message = UserContants.Message.NoInputData,
                    Status = CommandResponseStatus.Failed
                };
            }
            else
            {
                var user = Session.VietlottDbContext.Users.FirstOrDefault(p => p.Id.Equals(request.UserId));
                if (user == null)
                {
                    return new UserGetOneResponse
                    {
                        Message = UserContants.Message.UserNotFound,
                        Status = CommandResponseStatus.NotFound
                    };
                }
                return new UserGetOneResponse
                {
                    Message = UserContants.Message.GetUserOk,
                    Status = CommandResponseStatus.Ok,
                    User = Mapper.Map<UserModel>(user)
                };
            }

        }
    }
    public class UserGetOneRequest
    {
        public string UserId { get; set; }
    }
    public class UserGetOneResponse : CommandResponseBase
    {
        public UserModel User { get; set; }
    }
}
