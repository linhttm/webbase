﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Vietlott.Core.Caching;
using Vietlott.Core.Constants;
using Vietlott.Core.Extensions;
using Vietlott.Core.Interfaces;
using Vietlott.Core.Sessions;
using Vietlott.Data.Entities;

namespace Vietlott.Business.Users.Commands
{
    public interface IUserDeleteCommand : ICommand<UserDeleteRequest, UserDeleteResponse>
    {
    }
    public class UserDeleteCommand : SessionComponentBase, IUserDeleteCommand
    {
        public UserDeleteCommand(IAppSession session, ICacheEngine cacheEngine) : base(session, cacheEngine)
        {

        }
        public UserDeleteResponse Invoke(UserDeleteRequest request)
        {
            if (string.IsNullOrEmpty(request.UserId))
            {
                return new UserDeleteResponse
                {
                    Message = UserContants.Message.NoInputData,
                    Status = CommandResponseStatus.Failed
                };
            }
            else
            {
                var user = Session.VietlottDbContext.Users.FirstOrDefault(p => p.Id.Equals(request.UserId));
                if (user == null)
                {
                    return new UserDeleteResponse
                    {
                        Message = UserContants.Message.UserNotFound,
                        Status = CommandResponseStatus.NotFound
                    };
                }
                else
                {
                    Session.VietlottDbContext.Users.Remove(user);
                    return new UserDeleteResponse
                    {
                        Message = UserContants.Message.UserDeleteOk,
                        Status = CommandResponseStatus.Ok
                    };
                }
            }
        }
    }
    public class UserDeleteRequest
    {
        public string UserId { get; set; }
    }
    public class UserDeleteResponse : CommandResponseBase
    {
    }
}
