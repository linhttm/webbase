﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vietlott.Business.Users.Models;
using Vietlott.Core.Caching;
using Vietlott.Core.Constants;
using Vietlott.Core.Extensions;
using Vietlott.Core.Interfaces;
using Vietlott.Core.Sessions;
using Vietlott.Core.Users;
using Vietlott.Data.Entities;

namespace Vietlott.Business.Users.Commands
{
    public interface IUserLoginCommand : ICommand<LoginRequest, LoginResponse>
    {

    }
    public class UserLoginCommand : SessionComponentBase, IUserLoginCommand
    {
        public UserLoginCommand(IAppSession session, ICacheEngine cacheEngine) : base(session, cacheEngine)
        {

        }

        public LoginResponse Invoke(LoginRequest request)
        {
            if (string.IsNullOrEmpty(request.Phone) || string.IsNullOrEmpty(request.Password))
            {
                return new LoginResponse
                {
                    Status = CommandResponseStatus.Failed,
                    Message = LoginConstants.Message.fail
                };
            }
            else
            {
                User user = Session.VietlottDbContext.Users.Where(x => x.Phone.Equals(request.Phone)).FirstOrDefault();
                bool pass = user != null && PasswordHash.ValidatePassword(request.Password, user.Password);
                if (user != null && pass)
                {
                    return new LoginResponse
                    {
                        Status = CommandResponseStatus.Ok,
                        Message = LoginConstants.Message.success,
                        User = Mapper.Map<UserModel>(user)
                    };
                }
                else
                {
                    return new LoginResponse
                    {
                        Status = CommandResponseStatus.NotFound,
                        Message = LoginConstants.Message.fail
                    };
                }
            }
        }
    }

    public class LoginRequest
    {
        public string Phone { get; set; }
        public string Password { get; set; }
    }

    public class LoginResponse : CommandResponseBase
    {
        public UserModel User { get; set; }
    }
}
