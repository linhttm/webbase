﻿using AutoMapper;
using log4net.Appender;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vietlott.Business.Users.Models;
using Vietlott.Core.Caching;
using Vietlott.Core.Constants;
using Vietlott.Core.Extensions;
using Vietlott.Core.Interfaces;
using Vietlott.Core.Sessions;
using Vietlott.Core.Users;
using Vietlott.Data.Entities;

namespace Vietlott.Business.Users.Commands
{
    public interface IUserChangePasswordCommand : ICommand<UserChangePasswordRequest, UserChangePasswordResponse>
    {

    }

    public class UserChangePasswordCommand : SessionComponentBase, IUserChangePasswordCommand
    {
        public UserChangePasswordCommand(IAppSession session, ICacheEngine cacheEngine) : base(session, cacheEngine)
        {

        }

        public UserChangePasswordResponse Invoke(UserChangePasswordRequest request)
        {
            if (string.IsNullOrEmpty(request.UserId) || string.IsNullOrEmpty(request.OldPassword) || string.IsNullOrEmpty(request.NewPassword) || string.IsNullOrEmpty(request.ConfirmPassword))
            {
                return new UserChangePasswordResponse
                {
                    Status = CommandResponseStatus.Failed,
                    Message = UserContants.Message.NoInputData
                };
            }
            else
            {
                var user = Session.VietlottDbContext.Users.Where(x => x.Id.Equals(request.UserId)).FirstOrDefault();
                if (user == null)
                {
                    return new UserChangePasswordResponse
                    {
                        Status = CommandResponseStatus.NotFound,
                        Message = UserContants.Message.UserNotFound
                    };
                }
                else
                {
                    if (!PasswordHash.ValidatePassword(request.OldPassword, user.Password))
                    {
                        return new UserChangePasswordResponse
                        {
                            Status = CommandResponseStatus.Failed,
                            Message = UserContants.Message.OldPassWrong
                        };
                    }
                    else
                    {
                        if (!request.NewPassword.Equals(request.ConfirmPassword))
                        {
                            return new UserChangePasswordResponse
                            {
                                Status = CommandResponseStatus.Failed,
                                Message = UserContants.Message.ConfirmPassWrong
                            };
                        }
                        else
                        {
                            user.Password = PasswordHash.CreateHash(request.NewPassword);
                            return new UserChangePasswordResponse
                            {
                                Status = CommandResponseStatus.Ok,
                                Message = UserContants.Message.ChangePassOk,
                                User = Mapper.Map<UserModel>(user)
                            };
                        }
                    }
                }
            }
        }
    }

    public class UserChangePasswordRequest
    {
        public string UserId { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
    }

    public class UserChangePasswordResponse : CommandResponseBase
    {
        public UserModel User { get; set; }
    }
}
