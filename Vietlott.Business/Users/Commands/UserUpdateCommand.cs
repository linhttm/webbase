using AutoMapper;
using System.Linq;
using Vietlott.Business.Users.Models;
using Vietlott.Core.Caching;
using Vietlott.Core.Constants;
using Vietlott.Core.Extensions;
using Vietlott.Core.Interfaces;
using Vietlott.Core.Sessions;
using Vietlott.Data.Entities;

namespace Vietlott.Business.Users.Commands
{
    public interface IUserUpdateCommand : ICommand<UserUpdateRequest, UserUpdateResponse>
    {
    }
    public class UserUpdateCommand : SessionComponentBase, IUserUpdateCommand
    {
        public UserUpdateCommand(IAppSession session, ICacheEngine cacheEngine) : base(session, cacheEngine)
        {
        }
        public UserUpdateResponse Invoke(UserUpdateRequest request)
        {
            if (string.IsNullOrEmpty(request.UserId) || string.IsNullOrEmpty(request.Name) || string.IsNullOrEmpty(request.Cmnd))
            {
                return new UserUpdateResponse
                {
                    Message = UserContants.Message.MissingInformation,
                    Status = CommandResponseStatus.Failed
                };
            }
            else
            {
                IQueryable<User> users = Session.VietlottDbContext.Users;
                User user = users.Where(x => x.Id.Equals(request.UserId)).FirstOrDefault();
                if (user == null)
                {
                    return new UserUpdateResponse
                    {
                        Message = UserContants.Message.UserNotFound,
                        Status = CommandResponseStatus.NotFound
                    };
                }
                else
                {
                    user.Name = request.Name;
                    user.Cmnd = request.Cmnd;
                    return new UserUpdateResponse
                    {
                        Message = UserContants.Message.UserUpdateOk,
                        Status = CommandResponseStatus.Ok,
                        User = Mapper.Map<UserModel>(user)
                    };
                }
            }
        }
    }
    public class UserUpdateRequest
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Cmnd { get; set; }
    }
    public class UserUpdateResponse : CommandResponseBase
    {
        public UserModel User { get; set; }
    }
}
