﻿using AutoMapper;
using System.Linq;
using Vietlott.Business.Users.Models;
using Vietlott.Core.Caching;
using Vietlott.Core.Constants;
using Vietlott.Core.Extensions;
using Vietlott.Core.Interfaces;
using Vietlott.Core.Sessions;
using Vietlott.Core.Users;
using Vietlott.Data.Entities;

namespace Vietlott.Business.Users.Commands
{
    public interface IUserRegisterCommand : ICommand<RegisterRequest, RegisterReponse>
    {
    }
    public class UserRegisterCommand : SessionComponentBase, IUserRegisterCommand
    {
        public UserRegisterCommand(IAppSession session, ICacheEngine cacheEngine) : base(session, cacheEngine)
        {

        }
        public RegisterReponse Invoke(RegisterRequest request)
        {
            if (string.IsNullOrEmpty(request.User.Name) || string.IsNullOrEmpty(request.User.Phone) || string.IsNullOrEmpty(request.User.Cmnd) || string.IsNullOrEmpty(request.User.Password))
            {
                return new RegisterReponse
                {
                    Status = CommandResponseStatus.Failed,
                    Message = UserContants.Message.MissingInformation
                };
            }
            else
            {
                IQueryable<User> users = Session.VietlottDbContext.Users;
                var user = users.Where(x => x.Phone.Equals(request.User.Phone)).FirstOrDefault();
                if (user != null)
                {
                    return new RegisterReponse
                    {
                        Status = CommandResponseStatus.Failed,
                        Message = UserContants.Message.PhoneAlreadyExists
                    };
                }
                else
                {
                    request.User.Password = PasswordHash.CreateHash(request.User.Password);
                    user = Mapper.Map<User>(request.User);
                    Session.VietlottDbContext.Users.Add(user);
                    return new RegisterReponse
                    {
                        Status = CommandResponseStatus.Ok,
                        Message = UserContants.Message.SignUpSuccess,
                        User = Mapper.Map<UserModel>(user)
                    };
                }
            }
        }
    }
    public class RegisterRequest
    {
        public User User { get; set; }
    }
    public class RegisterReponse : CommandResponseBase
    {
        public UserModel User { get; set; }
    }
}
