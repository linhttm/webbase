﻿using Vietlott.Core.Encryption;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;

namespace Vietlott.Core.Configurations
{
    public class SystemConfiguration
    {
        private static SystemConfiguration _instance;
        private static IConfiguration _config;
        private readonly IEncryptor _encryptor;

        public IConfiguration Config { get; }
        private string _encryptedDatabaseConnectionString { get; set; }
        public string DatabaseConnectionString { get; private set; }

        public string RootFolder { get; private set; }
        public string AppName { get; private set; }
        public string VietlottUrl { get; private set; }
        public string ServiceAppName { get; private set; }
        public string EmailAppName { get; private set; }

        public string JWTSecretKey { get; set; }
        public string TokenLifeTimeMinutes { get; set; }

        public string RedisServer { get; set; }

        public string _encryptedAWSAccessKey { get; set; }
        public string AWSAccessKey { get; set; }

        private string _encryptedAWSSecretKey { get; set; }
        public string AWSSecretKey { get; set; }

        public string Bucket { get; set; }
        public string AmazonLink { get; set; }
        public string BucketRegion { get; set; }
        public string EmailBucketRegion { get; set; }
        public string AWSSQSEndpoint { get; set; }
        public string AwsSender { get; set; }
        public string AwsDisplayName { get; set; }

        public string MediaFolder { get; set; }
        public bool AutoTest { get; set; }

        private SystemConfiguration()
        {
            _config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Development"}.json").Build();

            var serviceCollection = new ServiceCollection();
            var services = serviceCollection.BuildServiceProvider();
            _encryptor = ActivatorUtilities.CreateInstance<RijndaelEncryptor>(services);

            Config = _config;

            GetConfigs();
        }
        public static SystemConfiguration Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SystemConfiguration();
                }
                return _instance;
            }
        }

        public static class UrlHelper
        {
            public static string VietlottUrl()
            {
                return Instance.VietlottUrl;
            }
        }

        private void GetConfigs()
        {
            DatabaseConnectionString = _config["Database:ConnectionString"];

            RootFolder = _config["RootFolder"];
            AppName = _config["AppName"];

            JWTSecretKey = _config["JWTToken:SecretKey"];
            TokenLifeTimeMinutes = _config["JWTToken:TokenLifeTimeMinutes"];

            VietlottUrl = _config["VietlottUrl"];

            _encryptedAWSAccessKey = _config["AmazonWebService:AWSAccessKey"];
            AWSAccessKey = _encryptor.Decrypt(_encryptedAWSAccessKey);

            _encryptedAWSSecretKey = _config["AmazonWebService:AWSSecretKey"];
            AWSSecretKey = _encryptor.Decrypt(_encryptedAWSSecretKey);

            AWSSQSEndpoint = _config["AmazonWebService:AWSSQSEndPoint"];
            Bucket = _config["AmazonWebService:Bucket"];
            AWSSQSEndpoint = _config["AmazonWebService:AWSSQSEndPoint"];
            BucketRegion = _config["AmazonWebService:RegionEndpoint"];
            EmailBucketRegion = _config["AmazonWebService:EmailRegionEndpoint"];
            AwsSender = _config["AmazonWebService:Sender"];
            AwsDisplayName = _config["AmazonWebService:DisplayName"];
            AWSSQSEndpoint = _config["AmazonWebService:AWSSQSEndPoint"];
            AmazonLink = _config["AmazonWebService:AmazonLink"];

            MediaFolder = _config["MediaFolder"];

            ServiceAppName = _config["ServiceAppName"];
            EmailAppName = _config["EmailAppName"];

            AutoTest = _config["AutoTest"]?.ToLower() == "true";
        }
    }
}
