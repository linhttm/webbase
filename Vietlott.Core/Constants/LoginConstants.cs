﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vietlott.Core.Constants
{
    public class LoginConstants
    {
        public static class Message
        {
            public const string success = "Logged in successfully";
            public const string fail = "User account or password is incorrect";
        }
    }
}
