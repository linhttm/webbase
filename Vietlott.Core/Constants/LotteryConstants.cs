﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vietlott.Core.Constants
{
    public class LotteryConstants
    {
        public static class Message
        {
            public const string History = "There is history";
            public const string NoHistory = "There is no history";
            public const string FailedHistory = "There is a fault";

            public const string SubmitLotteryOk = "Submit successfully";
            public const string SubmitLotteryFailed = "Submit failed";
        }
    }
}
