﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vietlott.Core.Constants
{
    public class UserContants
    {
        public static class Message
        {
            public const string MissingInformation = "Enter the missing user information";

            public const string UsernameAlreadyExists = "The username already exists";
            public const string PhoneAlreadyExists = "Phone number already exists";

            public const string SignUpSuccess = "Sign Up Success";
            public const string UserNotFound = "User not found";

            public const string UserUpdateOk = "Update user successfully";
            public const string UserDeleteOk = "Successfully deleted the user";

            public const string GetUserOk = "Retrieve user successfully";
            public const string GetUsersOk = "Retrieve all users successfully";

            public const string NoInputData = "There is no input data";

            public const string OldPassWrong = "The old password is incorrect";
            public const string ConfirmPassWrong = "The confirmation password is incorrect";
            public const string ChangePassOk = "Change password successfully";
        }
    }
}
