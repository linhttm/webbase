﻿using Newtonsoft.Json;
using System;

namespace Vietlott.Core.Extensions
{
    [Serializable]
    public class MyException : Exception
    {
        /// <summary>
        /// Flag indicating whether the exception has been logged
        /// </summary>
        public bool Logged { get; set; }

        public MyException()
        { }

        public MyException(string message)
            : this(message, false)
        { }

        public MyException(string message, bool logged)
            : base(message)
        {
            Logged = logged;
        }

        public MyException(string message, bool logged, Exception innerException)
            : base(message, innerException)
        {
            Logged = logged;
        }

        public MyException WithData(string key, string value)
        {
            if (!Data.Contains(key))
            {
                Data.Add(key, value);
            }

            return this;
        }

        public MyException WithDataObj<T>(string key, T value)
        {
            if (!Data.Contains(key))
            {
                Data.Add(key, JsonConvert.SerializeObject(value));
            }

            return this;
        }
    }
}
