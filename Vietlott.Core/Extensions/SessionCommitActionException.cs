﻿using System;

namespace Vietlott.Core.Extensions
{
    [Serializable]
    public class SessionCommitActionException : MyException
    {
        public SessionCommitActionException()
        { }

        public SessionCommitActionException(string message)
            : base(message)
        { }

        public SessionCommitActionException(string message, bool logged)
            : base(message, logged)
        { }

        public SessionCommitActionException(string message, bool logged, Exception innerException)
            : base(message, logged, innerException)
        { }
    }
}
