﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Vietlott.Core.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddScopeByConvention(this IServiceCollection services, Assembly assembly, Func<Type, bool> interfacePredicate, Func<Type, bool> implementationPredicate)
        {
            var interfaces = assembly.ExportedTypes
                .Where(x => x.IsInterface && interfacePredicate(x)).ToList();

            var implementations = assembly.ExportedTypes.Where(x => !x.IsInterface && !x.IsAbstract && implementationPredicate(x)).ToList();

            foreach (var @interface in interfaces)
            {
                var implementation = implementations.FirstOrDefault(x => @interface.IsAssignableFrom(x));

                if (implementation == null)
                {
                    continue;
                }

                services.AddScoped(@interface, implementation);
            }

            return services;
        }

        public static IServiceCollection AddScopesByConvention(this IServiceCollection services, Assembly assembly, Func<Type, bool> predicate)
           => services.AddScopeByConvention(assembly, predicate, predicate);

        public static IServiceCollection AddWebDataLayer(this IServiceCollection services)
        {
            var assemblies = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.dll")
                        .Select(x => Assembly.Load(AssemblyName.GetAssemblyName(x)));

            var interfaceAssemblies = new[] { typeof(ServiceCollection).GetTypeInfo().Assembly, typeof(IQueryable).GetTypeInfo().Assembly };

            foreach (var assembly in assemblies.Where(m => m.FullName.Contains("Vietlott")))
            {
                foreach (var interfaceAssembly in interfaceAssemblies)
                {
                    services.AddScopesByConvention(assembly, x => x.Name.EndsWith("Query") || x.Name.EndsWith("Command") || x.Name.EndsWith("Builder") && !x.Name.Contains("Singleton"));
                }
            }

            return services;
        }
    }
}
