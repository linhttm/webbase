﻿using HashidsCore.NET;
using System;

namespace Vietlott.Core.Extensions
{
    public static class StringExtensions
    {
        public static bool IsNullOrEmpty(this string s)
        {
            return string.IsNullOrEmpty(s);
        }

        public static bool IsNotNullOrEmpty(this string s)
        {
            return !string.IsNullOrEmpty(s);
        }

        public static string FormatWith(this string s, params object[] args)
        {
            return string.Format(s, args);
        }

        public static double ToDoubleOrDefault(this string s, int defaultValue = -1)
        {
            double result;
            if (!double.TryParse(s, out result))
            {
                return defaultValue;
            }

            return result;
        }

        public static string GetRandomString()
        {
            return (new Hashids(Guid.NewGuid().ToString("N"))).Encode(1, 2, 3, 4);
        }
    }
}
