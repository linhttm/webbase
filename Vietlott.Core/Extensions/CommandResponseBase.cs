﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vietlott.Core.Extensions
{
    public enum CommandResponseStatus
    {
        Ok,
        Failed,
        NotFound
    }
    public class CommandResponseBase
    {
        public CommandResponseStatus Status { get; set; }
        public string Message { get; set; }
    }
}
