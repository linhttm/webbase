﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vietlott.Core.Extensions
{
    public class ResponseMessageBase
    {
        public ResponseMessageBase(string message)
        {
            Message = message;
        }

        public string Message { get; set; }
    }

    public class ResponseMessageBase<T>
    {
        public ResponseMessageBase(string message, T data)
        {
            Data = data;
            Message = message;
        }

        public string Message { get; set; }
        public T Data { get; set; }
    }

}
