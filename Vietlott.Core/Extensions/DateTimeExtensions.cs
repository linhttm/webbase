﻿using System;
using System.Globalization;
using System.Linq;

namespace Vietlott.Core.Extensions
{
    public static class DateTimeExtensions
    {
        private static readonly DateTime _epoch = new DateTime(1970, 1, 1);

        public static DateTime RangeEnd(this DateTime date)
        {
            //this is designed for use in an "end of range" search; i.e. if you have specified the date
            //24 May 2012, what you actually mean is the end of that day, however if you've specified a
            //time, you actually mean that time
            if (date.Hour == 0 && date.Minute == 0 && date.Second == 0)
            {
                return date.AddDays(1);
            }

            return date;
        }

        public static long ConvertToUnixTimestamp(this DateTime dateTime)
        {
            var ts = new TimeSpan(dateTime.Ticks - _epoch.Ticks);
            return (long)ts.TotalMilliseconds;
        }

        public static string GetMonthText(int month)
        {
            return new DateTime(2000, month, 1).ToString("MMM").ToUpperInvariant();
        }

        public static bool IsBetween(this DateTime utc, DateTimeOffset start, DateTimeOffset end)
        {
            return start.UtcDateTime <= utc && end.UtcDateTime >= utc;
        }

        public static bool IsBetween(this DateTime utc, DateTime start, DateTime end)
        {
            return start <= utc && end >= utc;
        }

        public static DateTime ConvertFromUnixTimestamp(this long ms)
        {
            return _epoch.AddMilliseconds(ms);
        }
        public static DateTime ConvertFromUnixTimestampToDatetime(this long ms)
        {
            return _epoch.AddSeconds(ms);
        }
        public static DateTime GetDateFromYYYYMMDD(this string input)
        {
            if (input == null || input.Length != 8)
            {
                throw new InvalidOperationException($"Incorrect format {input ?? string.Empty}, YYYYMMDD expected");
            }

            return new DateTime(int.Parse(input.Substring(0, 4)), int.Parse(input.Substring(4, 2)), int.Parse(input.Substring(6, 2)));
        }
        public static DateTime GetDateFromString(this string input)
        {
           return DateTime.ParseExact(input, "dd/MM/yyyy", CultureInfo.InvariantCulture);
        }
        public static bool TryParseString(this string input)
        {
            return !DateTime.TryParseExact(input, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out _);
        }
        public static DateTime? TryParse(this string date)
        {
            if (date.IsNullOrEmpty())
            {
                return null;
            }

            DateTime dt;
            if (DateTime.TryParse(date, out dt))
            {
                return dt.Date;
            }

            return null;
        }

        public static double MinutesBehindNow(this DateTime dt)
        {
            return (DateTime.UtcNow - dt).TotalMinutes;
        }

        public static double MinutesBehindNow(this DateTimeOffset dt)
        {
            return (DateTimeOffset.UtcNow - dt).TotalMinutes;
        }

        public static string ToGoldrushFormat(this DateTimeOffset datetime)
        {
            return datetime.ToString("ddd MMM dd yyyy HH:mm:ss \"GMT\"zzzz", CultureInfo.InvariantCulture);
        }

        public static string ToFolder(this DateTimeOffset datetime)
        {
            return datetime.ToString("dd-MM-yyyy");
        }

        public static string ToFolder(this DateTime datetime)
        {
            return datetime.ToString("dd-MM-yyyy");
        }
        public static string ToFormatString(this DateTime datetime)
        {
            return datetime.ToString("dd/MM/yyyy");
        }
        public static string ToFriendlyDateTimeFormat(this DateTime datetimeUtc)
        {
            return datetimeUtc.ToString("d MMM yyyy HH:mm:ss");
        }
        public static string ToDateTimeUtcFormat(this DateTime datetimeUtc)
        {
            return datetimeUtc.ToString("yyyy-MM-ddTHH:mm:ss.fffffffZ");
        }

        public static string ToFriendlyDateTimeMinuteFormat(this DateTime datetimeUtc)
        {
            return datetimeUtc.ToString("d MMM yyyy HH:mm");
        }

        public static string ToTimeFormat(this DateTime datetimeUtc)
        {
            return datetimeUtc.ToString("HH:mm");
        }

        public static string ToFriendlyDateFormat(this DateTime datetimeUtc)
        {
            return datetimeUtc.ToString("d MMM yyyy");
        }

        public static string ToBasDateFormat(this DateTime datetimeUtc)
        {
            return datetimeUtc.ToString("MMM yyyy");
        }

        public static string ToFormatedString(this DateTimeOffset datetime)
        {
            return "{0:yyyy-MM-ddTHH:mm:sszzz}".FormatWith(datetime);
        }

        public static bool IsHoliday(this DateTime date, DateTime[] holidays)
        {
            return holidays.Any(d => d.Date == date.Date);
        }

        public static bool IsWeekend(this DateTime date)
        {
            DayOfWeek day = date.DayOfWeek;
            return day == DayOfWeek.Saturday || day == DayOfWeek.Sunday;
        }

        public static bool IsWeekDay(this DayOfWeek day, bool includeSaturday = false)
        {
            return (includeSaturday || day != DayOfWeek.Saturday) && day != DayOfWeek.Sunday;
        }
    }
}
