﻿namespace Vietlott.Core.Encryption
{
    public interface IEncryptor
    {
        /// <summary>
        /// Decrypts a string 
        /// </summary>
        /// <returns>the decrypted value of the string</returns>
        string Decrypt(string encryptedText);

        /// <summary>
        /// Encrypts a string
        /// </summary>
        /// <returns>the encrypted value of the string</returns>
        string Encrypt(string text);
    }
}
