﻿using Vietlott.Core.Sessions.Actions;
using Vietlott.Data.Entities;
using System;
using System.Threading.Tasks;

namespace Vietlott.Core.Sessions
{
    public interface IAppSession : IDisposable
    {
        /// <summary>
        /// Access to the master database
        /// </summary>
        VietlottDbContext VietlottDbContext { get; set; }
        /// <summary>
        /// If true (the default) all requests go to master otherwise the session will round robin reads to all servers
        /// </summary>
        bool ReadOnlySession { get; set; }
        /// <summary>
        /// Saves changes within the session and executes all session flush actions
        /// </summary>
        Task Commit();
        /// <summary>
        /// Clear down the list of actions to invoke on commit.
        /// </summary>
        void ClearCommitActions();
        /// <summary>
        /// Action to execute if the SaveChanges on CentralPower database fails
        /// </summary>
        /// <param name="action"></param>
        void AddRollbackAction(Action action);
        /// <summary>
        /// If we need to execute the rollback actions associated with this session when the commit does not fail
        /// </summary>
        void ExecuteRollbackActions();
        /// <summary>
        /// Add an event message to send to message queue on successful session commit
        /// </summary>
        /// <param name="sessionCommitAction">Teh session flush action.</param>
        void AddCommitAction(SessionCommitAction sessionCommitAction);

    }
}
