﻿using System;

namespace Vietlott.Core.Sessions.Actions
{
    public abstract class SessionCommitAction : ComponentBase
    {
        public virtual bool IsBulkMessageSender => false;
        public abstract void Execute(IAppSession session);

        public virtual void HandleError(IAppSession session, Exception e)
        { }
    }
}
