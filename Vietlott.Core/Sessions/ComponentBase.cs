﻿using log4net;
using System;

namespace Vietlott.Core.Sessions
{
    public abstract class ComponentBase
    {
        private readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void Trace(string message, params object[] args)
        {
            Console.WriteLine(message, args);
            _log.InfoFormat(message, args);
        }

        public void Info(string message, params object[] args)
        {
            _log.Info(message);
        }

        public void Warning(string message, params object[] args)
        {
            _log.WarnFormat(message, args);
        }

        public void Error(Exception e)
        {
            _log.Error(e.Message, e);
        }

        public void Error(string message, params object[] args)
        {
            _log.Error(message);
        }

        public T TryGet<T>(Func<T> action) where T : class
        {
            try
            {
                return action();
            }
            catch (Exception e)
            {
                Error(e);
                return null;
            }
        }
    }
}
