﻿using Vietlott.Core.Extensions;
using Vietlott.Core.Sessions.Actions;
using Vietlott.Data.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Vietlott.Core.Sessions
{
    public class AppSession : ComponentBase, IAppSession
    {
        private readonly List<SessionCommitAction> _sessionCommitActions;
        private readonly List<Action> _rollbackActions;
        public VietlottDbContext VietlottDbContext { get; set; }
        public bool ReadOnlySession { get; set; }
        public AppSession(
            VietlottDbContext dbContext)
        {
            VietlottDbContext = dbContext;
            _sessionCommitActions = new List<SessionCommitAction>();
            _rollbackActions = new List<Action>();
        }

        protected IEnumerable<SessionCommitAction> SessionCommitActions => _sessionCommitActions;
       
        public void Dispose()
        {
            VietlottDbContext.Dispose();
            _sessionCommitActions.Clear();
            _rollbackActions.Clear();
        }

        public void ExecuteRollbackActions()
        {
            foreach (var rollbackAction in _rollbackActions)
            {
                try
                {
                    rollbackAction.Invoke();
                }
                catch (Exception ex)
                {
                    Error(new SessionCommitActionException("Failed to execute rollback action", true, ex));
                }
            }

            _rollbackActions.Clear();
        }

        public async Task Commit()
        {
            if (!ReadOnlySession)
            {
                try
                {
                    await VietlottDbContext.SaveChangesAsync();
                }
                catch
                {
                    ExecuteRollbackActions();
                    throw;
                }

                foreach (var sessionCommitAction in _sessionCommitActions)
                {
                    try
                    {
                        sessionCommitAction.Execute(this);
                    }
                    catch (Exception ex)
                    {
                        try
                        {
                            sessionCommitAction.HandleError(this, ex);
                        }
                        catch (Exception inner)
                        {
                            Error(new SessionCommitActionException($"Failed to execute session commit action error handler: {sessionCommitAction.GetType().FullName}", true, inner));
                        }

                        Error(new SessionCommitActionException($"Failed to execute session commit action: {sessionCommitAction.GetType().FullName}", true, ex));
                    }
                }

                _sessionCommitActions.Clear();
                _rollbackActions.Clear();
            }
        }

        public void AddCommitAction(SessionCommitAction sessionCommitAction)
        {
            _sessionCommitActions.Add(sessionCommitAction);
        }

        public void ClearCommitActions()
        {
            _sessionCommitActions.Clear();
        }

        public void AddRollbackAction(Action action)
        {
            _rollbackActions.Add(action);
        }
    }
}
