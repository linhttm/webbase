using Vietlott.Core.Caching;

namespace Vietlott.Core.Sessions
{
    public abstract class SessionComponentBase : ComponentBase
    {
        public SessionComponentBase(IAppSession session, ICacheEngine cacheEngine)
        {
            Session = session;
            CacheEngine = cacheEngine;
        }

        public IAppSession Session { protected get; set; }
        public ICacheEngine CacheEngine { protected get; set; }
    }
}
