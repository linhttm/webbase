﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vietlott.Core.Interfaces
{
    public interface IQuery<in TRequest, out TResponse> : IWorkflow<TRequest, TResponse>
    { }

    public interface IQuery<out TResponse>
    {
        TResponse Invoke();
    }

    public interface IWorkflow<in TRequest, out TResponse>
    {
        TResponse Invoke(TRequest request);
    }
}
