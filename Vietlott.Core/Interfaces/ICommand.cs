﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Vietlott.Core.Interfaces
{
    public interface ICommand<in TRequest, out TResponse> : IWorkflow<TRequest, TResponse>
    { }

    public interface ICommand<in TRequest>
    {
        void Invoke(TRequest request);
    }

    public interface ICommand
    {
        void Invoke();
    }

    public interface IAsyncCommand<in TRequest, TResponse>
    {
        Task<TResponse> Invoke(TRequest request);
    }

    public interface IAsyncCommand<in TRequest>
    {
        Task Invoke(TRequest request);
    }

    public interface IAsyncCommand
    {
        Task Invoke();
    }
}
