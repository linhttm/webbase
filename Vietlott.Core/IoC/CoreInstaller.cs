﻿using Vietlott.Core.Caching;
using Vietlott.Core.Sessions;
using Microsoft.Extensions.DependencyInjection;

namespace Vietlott.Core.IoC
{
    public class CoreInstaller
    {
        public static void Install(IServiceCollection services)
        {
            services.AddMemoryCache();
            services.AddSingleton<ICacheEngine, InMemoryCacheEngine>();
            services.AddScoped<IAppSession, AppSession>();
        }
    }
}
