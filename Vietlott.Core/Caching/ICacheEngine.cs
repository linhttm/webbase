﻿using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace Vietlott.Core.Caching
{
    public interface ICacheEngine
    {
        Task<T> GetAsync<T>(CacheNamespace cacheNamespace, string key, Func<Task<T>> getter, TimeSpan? expires = null) where T : class;
        Task RefreshAsync(string key);
        Task PutAsync<T>(CacheNamespace cacheNamespace, string key, T item, TimeSpan? expires = null) where T : class;
        Task ClearAsync(CacheInvalidationMessage message);
        Task ClearAsync(CacheNamespace cache, string key);
        Task FlushAsync();
    }

    public enum CacheNamespace
    {
        General,
        Role,       
        User       
    }

    public class WellknownCacheKey
    {

    }

    public class CacheInvalidationMessage
    {
        public string SourceMachineName { get; set; }
        public CacheNamespace? Namespace { get; set; }
        public string Prefix { get; set; }
        public string Key { get; set; }
        public string[] Keys { get; set; }
        public bool ClearOutputCache { get; set; }

        public CacheInvalidationMessage()
        {
            Keys = new string[0];
        }

        public string Serialised()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
