﻿using Vietlott.Core.Extensions;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Vietlott.Core.Caching
{
    public class InMemoryCacheEngine : ICacheEngine
    {
        private readonly IMemoryCache _cache;
        private readonly List<string> Keys;
        private static readonly object _locker = new object();

        public InMemoryCacheEngine(IMemoryCache cache)
        {
            _cache = cache;
            Keys = new List<string>();
        }

        private async Task ClearKey(string key)
        {
            if (Keys.Contains(key))
            {
                await Task.Run(() =>
                {
                    lock (_locker)
                    {
                        _cache.Remove(key);
                        Keys.Remove(key);
                    }
                });
            }
        }

        public async Task ClearAsync(CacheInvalidationMessage message)
        {
            //if there is no namespace delete everything
            if (message.Namespace == null)
            {
                await FlushAsync();
            }
            else
            {
                //if there is no key we delete the entire namespace
                if (message.Key.IsNullOrEmpty() && !message.Keys.Any())
                {
                    var prefix = message.Prefix.IsNullOrEmpty() ? message.Namespace.Value.ToString().ToLowerInvariant() : GetCacheKey(message.Namespace.Value, message.Prefix);

                    for (var i = 0; i < Keys.Count; i++)
                    {
                        if (Keys[i].StartsWith(prefix))
                        {
                            await ClearKey(Keys[i]);
                            i--;
                        }
                    }
                }
                else if (message.Key.IsNotNullOrEmpty())
                {
                    await ClearKey(GetCacheKey(message.Namespace.Value, message.Key));
                }
                else if (message.Keys.Any())
                {
                    foreach (var key in message.Keys)
                    {
                        await ClearKey(GetCacheKey(message.Namespace.Value, key));
                    }
                }
            }
        }

        public async Task ClearAsync(CacheNamespace cache, string key)
        {
            await ClearKey(GetCacheKey(cache, key));
        }

        public async Task FlushAsync()
        {
            if (!Keys.IsNullOrEmpty())
            {
                for (var i = 0; i < Keys.Count; i++)
                {
                    await ClearKey(Keys[i]);
                    i--;
                }
            }
        }

        private string GetCacheKey(CacheNamespace cacheNamespace, string key)
        {
            return "{0}_{1}".FormatWith(cacheNamespace, key).ToLowerInvariant();
        }

        public async Task<T> GetAsync<T>(CacheNamespace cacheNamespace, string key, Func<Task<T>> getter, TimeSpan? expires = null) where T : class
        {
            T value = null;
            if (Keys.Contains(GetCacheKey(cacheNamespace, key)))
            {
                value = _cache.Get(GetCacheKey(cacheNamespace, key)) as T;
            }

            if (value == null)
            {
                value = await getter();
                if (value != null)
                {
                    await PutAsync(cacheNamespace, key, value, expires);
                }
            }
            return value;
        }

        public async Task PutAsync<T>(CacheNamespace cacheNamespace, string key, T item, TimeSpan? expires = null) where T : class
        {
            await Task.Run(() =>
            {
                lock (_locker)
                {
                    _cache.Set(GetCacheKey(cacheNamespace, key), item, expires ?? TimeSpan.FromDays(1));
                    if (!Keys.Contains(GetCacheKey(cacheNamespace, key)))
                    {
                        Keys.Add(GetCacheKey(cacheNamespace, key));
                    }
                }
            });
        }

        public async Task RefreshAsync(string key)
        {
            //nothing
        }
    }
}
