﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Vietlott.Api.Models
{
    public class UserUpdateViewModel
    {
        public string Name { get; set; }
        public string Cmnd { get; set; }
    }
}
