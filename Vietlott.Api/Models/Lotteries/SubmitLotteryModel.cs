﻿using System.Collections.Generic;
using Vietlott.Business.Lotteries.Models;

namespace Vietlott.Api.Models.Lotteries
{
    public class SubmitLotteryModel
    {
        public List<List<int>> TicketNumbers { get; set; }
        public UserSubmitViewModel UserModel { get; set; }
        public List<string> LotteryPeriod { get; set; }
    }
}
