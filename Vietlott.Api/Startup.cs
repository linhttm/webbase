using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.IO;
using System.Reflection;
using Vietlott.Api.ActionFilters;
using Vietlott.Api.IoC;
using Vietlott.Api.Mappers;
using Vietlott.Core.Configurations;
using Vietlott.Core.Extensions;
using Vietlott.Core.IoC;
using Vietlott.Data.Entities;

namespace Vietlott.Api
{
    public class Startup
    {
        private static bool MapperInitialized { get; set; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options =>
            {
                options.OutputFormatters.Clear();
                options.Filters.Add(typeof(IncomingRequestFilter));
            }).SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            services.AddDbContext<VietlottDbContext>(o =>
                        o.UseNpgsql(SystemConfiguration.Instance.DatabaseConnectionString));

            SetAuthorizeWithJWT(services);

            services.AddCors();

            services.AddMvc();

            services.AddControllers().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            }).SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            services.AddWebDataLayer();

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Web base API",
                    Description = "ASP.NET Core Web API for ..."
                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
                c.CustomSchemaIds(x => x.FullName);
            });

            ApiInstaller.Install(services);
            CoreInstaller.Install(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            //Initialise Automapper
            InitializeAutoMapper();

            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "Highlands LLEN API V1"); });

            app.UseRouting();

            // Enable cors from anywhere. 
            app.UseCors(b => b
               .AllowAnyOrigin()
               .AllowAnyHeader()
               .AllowAnyMethod());
            //.AllowCredentials());

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            UpdateDatabase(app);
        }
        private static void InitializeAutoMapper()
        {
            if (!MapperInitialized)
            {
                Mapper.Initialize(x => x.AddProfile<MappingProfiles>());
                MapperInitialized = true;
            }
        }
        private static void UpdateDatabase(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetService<VietlottDbContext>())
                {
                    context.Database.Migrate();
                    //DataSeeder.SeedData(context);
                    context.SaveChanges();
                }
            }
        }

        private static void SetAuthorizeWithJWT(IServiceCollection services)
        {
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.Authority = "https://securetoken.google.com/web-base-af4e7";
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    //ValidateIssuerSigningKey = true,
                    //IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SystemConfiguration.Instance.JWTSecretKey)),
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidAudience = "web-base-af4e7",
                    ValidIssuer = "https://securetoken.google.com/web-base-af4e7",
                    ValidateLifetime = true
                };
            });
        }
    }
}
