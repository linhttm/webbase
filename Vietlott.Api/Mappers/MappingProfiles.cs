using AutoMapper;
using Vietlott.Api.Models;
using Vietlott.Business.Lotteries.Models;
using Vietlott.Business.Users.Models;
using Vietlott.Core.Extensions;
using Vietlott.Core.Extensions;
using Vietlott.Data.Entities;

namespace Vietlott.Api.Mappers
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<UserSubmitViewModel, User>();
            CreateMap<User, UserSubmitViewModel>();
            CreateMap<User, RegisterViewModel>();

            CreateMap<RegisterViewModel, User>();

            CreateMap<RegisterViewModel, User>();

            CreateMap<UserModel, User>().ForMember(d => d.Id, o => o.MapFrom(s => s.UserId));
            CreateMap<User, UserModel>().ForMember(d => d.UserId, o => o.MapFrom(s => s.Id));

            CreateMap<User, UserModel>().ForMember(d => d.UserId, o => o.MapFrom(s => s.Id))
                .ForMember(d => d.CreatedDateUtc, o => o.MapFrom(s => string.Format("{0:dd-MM-yyyy}", s.CreatedDateUtc.Value.ConvertFromUnixTimestamp())));
        }
    }
}
