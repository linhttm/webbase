﻿using Vietlott.Core.Constants;
using Vietlott.Core.Sessions;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Vietlott.Api.ActionFilters
{
    public class IncomingRequestFilter : IAsyncActionFilter
    {
        private const string RESPONSE_TIME = "X-ResponseTime";
        private Stopwatch _watch;

        private readonly IAppSession _session;
        private readonly ILogger _logger;

        public IncomingRequestFilter(ILoggerFactory loggerFactory, IAppSession session)
        {
            _logger = loggerFactory.CreateLogger<IncomingRequestFilter>();
            _session = session;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            _session.ReadOnlySession = context.HttpContext.Request.Method == HttpConstants.HttpMethods.Get;

            _watch = Stopwatch.StartNew();

            // next() calls the action method.
            var resultContext = await next();

            // after the action executes.
            if (resultContext.Exception == null)
            {
                await _session.Commit();
            }
            else
            {
                _logger.LogError($"Uncaught exception error. Message: {resultContext.Exception.Message}. Inner message: {resultContext.Exception?.InnerException?.Message}", resultContext.Exception);
            }

            _watch.Stop();
            context.HttpContext.Response.Headers.Add(RESPONSE_TIME, _watch.Elapsed.ToString());
        }
    }
}
