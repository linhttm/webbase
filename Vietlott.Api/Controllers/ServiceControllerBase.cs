﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vietlott.Core.Caching;
using Vietlott.Core.Extensions;
using Vietlott.Core.Sessions;

namespace Vietlott.Api.Controllers
{
    public class ServiceControllerBase : ControllerBase
    {
        public IAppSession Session { get; set; }
        public ICacheEngine CacheEngine { get; set; }

        public ServiceControllerBase(IServiceProvider serviceProvider)
        {
            Session = serviceProvider.GetRequiredService<IAppSession>();
            CacheEngine = serviceProvider.GetRequiredService<ICacheEngine>();
        }

        public ServiceControllerBase() { }

        protected NotFoundObjectResult NotFoundError(string message = null)
        {
            return NotFound(new ResponseMessageBase(message));
        }

        protected BadRequestObjectResult BadRequestError(string message = null)
        {
            return BadRequest(new ResponseMessageBase(message));
        }

        protected BadRequestObjectResult BadRequestError<T>(T data, string message = null)
        {
            return BadRequest(new ResponseMessageBase<T>(message, data));
        }

        protected OkObjectResult Success(string message = null)
        {
            return Ok(new ResponseMessageBase(message));
        }

        protected OkObjectResult Success<T>(T data, string message = null)
        {
            return Ok(new ResponseMessageBase<T>(message, data));
        }
    }

}
