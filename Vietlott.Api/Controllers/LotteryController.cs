﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;
using Vietlott.Api.Models.Lotteries;
using Vietlott.Business.Lotteries.Commands;
using Vietlott.Business.Lotteries.Models;
using Vietlott.Business.Lotteries.Queries;
using Vietlott.Core.Extensions;
using Vietlott.Data.Entities;

namespace Vietlott.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LotteryController : ServiceControllerBase
    {
        private readonly ILotterySubmitCommand _lotterySubmitCommand;
        private readonly ILotteryHistoryQuery _lotteryHistoryQuery;
        public LotteryController(IServiceProvider serviceProvider,
            ILotterySubmitCommand lotterySubmitCommand, ILotteryHistoryQuery lotteryHistoryQuery
             ): base(serviceProvider)
        {
            _lotterySubmitCommand = lotterySubmitCommand;
            _lotteryHistoryQuery = lotteryHistoryQuery;
        }
        /// <summary> 
        /// LotteryPeriod Format: dd/MM/yyyy
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult<ResponseMessageBase<List<SubmitLotteryViewModel>>> Submit([FromBody] SubmitLotteryModel model)
        {
            var response = _lotterySubmitCommand.Invoke(new LotterySubmitRequest
            {
                LotteryPeriod = model.LotteryPeriod,
                TicketNumbers = model.TicketNumbers,
                User = Mapper.Map<User>(model.UserModel)
            });
            switch (response.Status)
            {
                case CommandResponseStatus.Ok:
                    return Success(response.LotteryModel, response.Message);
                case CommandResponseStatus.NotFound:
                    return NotFoundError(response.Message);
                case CommandResponseStatus.Failed:
                    return BadRequestError(response.Message);
                default: return BadRequestError(response.Message);
            }
        }
        /// <summary>
        /// Date Format: dd/MM/yyyy
        /// </summary>
        /// <param name="name"></param>
        /// <param name="phone"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<ResponseMessageBase<List<LotteryHistoryViewModel>>> LotteryHistory(string name, string phone, string date)
        {
            var response = _lotteryHistoryQuery.Invoke(new LotteryHistoryRequest
            {
                Name = name,
                Phone = phone,
                Date = date
            });
            switch (response.Status)  
            {
                case CommandResponseStatus.Ok:
                    return Success(response.Lotteries, response.Message);
                case CommandResponseStatus.NotFound:
                    return NotFoundError(response.Message);
                case CommandResponseStatus.Failed:
                    return BadRequestError(response.Message);
                default: return BadRequestError(response.Message);
            }
        }
    }
}
