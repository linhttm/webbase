﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Vietlott.Api.Models;
using Vietlott.Business.Lotteries.Models;
using Vietlott.Business.Lotteries.Queries;
using Vietlott.Business.Users.Commands;
using Vietlott.Business.Users.Models;
using Vietlott.Core.Extensions;
using Vietlott.Data.Entities;

namespace Vietlott.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ServiceControllerBase
    {
        private readonly IUserRegisterCommand _userRegisterCommand;
        private readonly IUserLoginCommand _userLoginCommand;
        private readonly IUserChangePasswordCommand _userChangePasswordCommand;
        private readonly IUserDeleteCommand _userDeleteCommand;
        private readonly IUserUpdateCommand _userUpdateCommand;
        private readonly IUserGetOneQuery _userGetOneQuery;
        private readonly IUserGetAllQuery _userGetAllQuery;
        private readonly IUserGetLotteryHistoryQuery _userGetLotteryHistoryQuery;
        public UsersController(
            IServiceProvider serviceProvider,
            IUserRegisterCommand userRegisterCommand,
            IUserChangePasswordCommand userChangePasswordCommand,
            IUserLoginCommand userLoginCommand,
            IUserDeleteCommand userDeleteCommand,
            IUserUpdateCommand userUpdateCommand,
            IUserGetOneQuery userGetOneQuery,
            IUserGetAllQuery userGetAllQuery,
            IUserGetLotteryHistoryQuery userGetLotteryHistoryQuery
        ) : base(serviceProvider)
        {
            _userRegisterCommand = userRegisterCommand;
            _userLoginCommand = userLoginCommand;
            _userChangePasswordCommand = userChangePasswordCommand;
            _userDeleteCommand = userDeleteCommand;
            _userUpdateCommand = userUpdateCommand;
            _userGetOneQuery = userGetOneQuery;
            _userGetAllQuery = userGetAllQuery;
            _userGetLotteryHistoryQuery = userGetLotteryHistoryQuery;
        }

        /// <summary>
        /// Register user
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("register")]
        public ActionResult<ResponseMessageBase<UserModel>> Register([FromBody] RegisterViewModel model)
        {
            var response = _userRegisterCommand.Invoke(new RegisterRequest
            {
                User = Mapper.Map<User>(model)
            });
            return response.Status switch
            {
                CommandResponseStatus.Ok => Success(response.User, response.Message),
                CommandResponseStatus.NotFound => NotFoundError(response.Message),
                CommandResponseStatus.Failed => BadRequestError(response.Message),
                _ => BadRequestError(response.Message),
            };
        }

        /// <summary>
        /// Login
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [HttpPost("login")]
        public ActionResult<ResponseMessageBase<UserModel>> Login([FromBody] LoginModel form)
        {
            var response = _userLoginCommand.Invoke(new LoginRequest
            {
                Phone = form.Phone,
                Password = form.Password
            });

            return response.Status switch
            {
                CommandResponseStatus.Ok => Success(response.User, response.Message),
                CommandResponseStatus.NotFound => NotFoundError(response.Message),
                CommandResponseStatus.Failed => BadRequestError(response.Message),
                _ => BadRequest(response.Message),
            };
        }

        /// <summary>
        /// Change password user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("{userId}/password")]
        public ActionResult<ResponseMessageBase<UserModel>> ChangePassword(string userId, [FromBody] ChangePasswordModel model)
        {
            var response = _userChangePasswordCommand.Invoke(new UserChangePasswordRequest
            {
                UserId = userId,
                OldPassword = model.OldPassword,
                NewPassword = model.NewPassword,
                ConfirmPassword = model.ConfirmPassword
            });

            return response.Status switch
            {
                CommandResponseStatus.Ok => Success(response.User, response.Message),
                CommandResponseStatus.NotFound => NotFoundError(response.Message),
                CommandResponseStatus.Failed => BadRequestError(response.Message),
                _ => BadRequestError(response.Message),
            };
        }

        /// <summary>
        /// Delete a user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpDelete("{userId}")]
        public ActionResult<ResponseMessageBase> Delete(string userId)
        {
            var response = _userDeleteCommand.Invoke(new UserDeleteRequest
            {
                UserId = userId
            });
            return response.Status switch
            {
                CommandResponseStatus.Ok => Success(response.Message),
                CommandResponseStatus.NotFound => NotFoundError(response.Message),
                CommandResponseStatus.Failed => BadRequestError(response.Message),
                _ => BadRequestError(response.Message),
            };
        }

        /// <summary>
        /// Update user (name, phone, cmnd)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("{userId}")]
        public ActionResult<ResponseMessageBase<UserModel>> Update(string userId, [FromBody] UserUpdateViewModel model)
        {
            var response = _userUpdateCommand.Invoke(new UserUpdateRequest
            {
                UserId = userId,
                Name = model.Name,
                Cmnd = model.Cmnd
            });

            return response.Status switch
            {
                CommandResponseStatus.Ok => Success(response.User, response.Message),
                CommandResponseStatus.NotFound => NotFoundError(response.Message),
                CommandResponseStatus.Failed => BadRequestError(response.Message),
                _ => BadRequestError(response.Message),
            };
        }

        /// <summary>
        /// Retrieve a user by id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet("{userId}")]
        public ActionResult<ResponseMessageBase<UserModel>> GetUser(string userId)
        {
            var response = _userGetOneQuery.Invoke(new UserGetOneRequest
            {
                UserId = userId
            });
            return response.Status switch
            {
                CommandResponseStatus.Ok => Success(response.User, response.Message),
                CommandResponseStatus.NotFound => NotFoundError(response.Message),
                CommandResponseStatus.Failed => BadRequestError(response.Message),
                _ => BadRequestError(response.Message),
            };
        }

        /// <summary>
        /// Retrieve multiple users by name, phone, cmnd
        /// </summary>
        /// <param name="name"></param>
        /// <param name="phone"></param>
        /// <param name="cmnd"></param>
        /// <returns></returns>
        [HttpGet()]
        public ActionResult<ResponseMessageBase<List<UserModel>>> GetUsers(string name, string phone, string cmnd)
        {
            var response = _userGetAllQuery.Invoke(new UserGetAllRequest
            {
                Name = name,
                Phone = phone,
                Cmnd = cmnd
            });
            return response.Status switch
            {
                CommandResponseStatus.Ok => Success(response.Users, response.Message),
                CommandResponseStatus.NotFound => NotFoundError(response.Message),
                CommandResponseStatus.Failed => BadRequestError(response.Message),
                _ => BadRequestError(response.Message),
            };
        }
        /// <summary>
        /// Get lottery ticket purchase history by user id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet("{userId}/lotteries")]
        public ActionResult<ResponseMessageBase<List<LotteryHistoryByUserViewModel>>> LotteryHistoryByUserId(string userId)
        {
            var response = _userGetLotteryHistoryQuery.Invoke(new UserGetLotteryHistoryRequest
            {
                UserId = userId
            });
            switch (response.Status)
            {
                case CommandResponseStatus.Ok:
                    return Success(response.Lotteries, response.Message);
                case CommandResponseStatus.NotFound:
                    return NotFoundError(response.Message);
                case CommandResponseStatus.Failed:
                    return BadRequestError(response.Message);
                default: return BadRequestError(response.Message);
            }
            return response.Status switch
            {
                CommandResponseStatus.Ok => Success(response.Lotteries, response.Message),
                CommandResponseStatus.NotFound => NotFoundError(response.Message),
                CommandResponseStatus.Failed => BadRequestError(response.Message),
                _ => BadRequestError(response.Message),
            };
        }
    }
}
